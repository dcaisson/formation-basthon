# Présentation de Basthon 

## Fonctionnement de Basthon
Pour utiliser Basthon, aucune installation n'est nécessaire : il suffit de disposer d'un navigateur et d'une connexion à Internet. Le code est intégralement exécuté par le navigateur.

Basthon permet donc de travailler avec une grande diversité d'appareils :

- un ordinateur ;  
- une tablette ;  
- un smartphone.  
  
## Créer un notebook 
Pour créer un notebook Jupyter à l'aide de Basthon, il suffit de se rendre sur le [site](https://basthon.fr) de Basthon puis de cliquer sur l'icône ``Notebook`` :

![icône notebook Basthon](images/icone_notebook_basthon.png){: .center width=80%}

## Partager un notebook 
Une fois le notebook créé, un lien permet de le partager :

![icône partager notebook](images/icone_partager_notebook.png){: .center width=80%}

??? warning "Attention !"
    Partager un notebook trop long peut ne pas fonctionner avec certains navigateurs (voir plus loin pour contourner ce problème) :  
    
    ![avertissement script trop long](images/avertissement_script_trop_long.png){: .center width=80%}
    
!!! danger "Remarque importante"
    Chaque personne utilisant un lien Basthon travaille sur **sa copie** du notebook partagé : lorsqu'on rafraîchit la page, toutes **les modifications faites sont perdues** et le notebook &laquo; original &raquo; est chargé à nouveau.

## Enregistrer ses modifications
Il est donc important de sauvegarder régulièrement son travail. Pour cela, il suffit de cliquer sur l'icône ![icône enregistrer de Basthon](images/icone_enregistrer.png).

!!! info "Remarque"
    Chaque clic sur l'icône précédente provoque le téléchargement du notebook : à la fin d'une session de travail, il y a donc autant de notebooks que de clics sur cette icône. Il suffit donc de **garder le dernier fichier téléchargé** et d'effacer les autres.

## Utiliser des fichiers annexes
Pour travailler avec une image, un fichier CSV etc stocké sur votre machine, il suffit de le charger dans Basthon :

![utiliser des fichiers annexes](images/utiliser_fichiers_annexes.png){: .center width=80%}

Il est ensuite possible d'ouvrir ce fichier avec Python, comme s'il était dans le répertoire courant.

??? hint "Astuce"
    Pour connaître la liste des fichiers chargés, on peut utiliser le module `os` dans une cellule du notebook :
    ```python
    import os
    os.listdir()
    ```

## Utiliser un module Python
Pour pouvoir importer votre propre module (contenu dans un fichier `*.py`), il suffit de l'ouvrir de la même façon qu'un fichier annexe, puis de choisir d'`installer le module` :

![installer un module dans Basthon](images/installer_module.png){: .center width=80%}

Basthon installe automatiquement les dépendances nécessaires puis vous notifie de la disponibilité du module :

![module disponible](images/module_disponible.png){: .center width=80%}

??? hint "Astuce"
    On peut à tout moment connaître la liste des modules disponibles (y compris les modules chargés de cette façon) en utilisant la commande `help('modules')`.
