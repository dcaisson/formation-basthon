# Aide-mémoire Markdown

Voici quelques éléments de syntaxe Markdown :

## :fontawesome-solid-heading: Titres

!!! abstract ""  
    ```
    # Titre de niveau 1
    ## Titre de niveau 2
    ### Titre de niveau 3
    #### Titre de niveau 4
    ##### Titre de niveau 5
    ###### Titre de niveau 6
    ```

## :fontawesome-solid-italic: Emphase

!!! abstract ""
    === "Markdown"
            *Texte en italique*
	        **Texte en gras**
		    ~~Texte barré~~
		    
    === "Aperçu"
        *Texte en italique*  
	    **Texte en gras**  
	        <s>Texte barré</s>

## :fontawesome-solid-bars: Listes


### :fontawesome-solid-list-ul: Liste à puces

!!! abstract ""
    === "Markdown"
            * Élément 1
            * Élément 2
                * Sous-élément 1
                * Sous élément 2
            * Élément 3 
    === "Aperçu"
        * Élément 1
        * Élément 2
            * Sous-élément 1
            * Sous élément 2
        * Élément 3 

### :fontawesome-solid-list-ol: Listes ordonnées

!!! abstract ""
    === "Markdown"
            1. Élément 1
            2. Élément 2
                1. Sous-élément 1
                2. Sous élément 2
            3. Élément 3 
    === "Aperçu"
        1. Élément 1
        2. Élément 2
            1. Sous-élément 1
            2. Sous élément 2
        3. Élément 3 

### :fontawesome-solid-check-square: Listes à cocher

!!! abstract ""
    === "Markdown"
            - [x] Élément 1, coché
            - [ ] Élément 2, non-coché
                - [x] Sous-élément 2.1, coché
                - [ ] Sous-élément 2.2, non-coché
            - [ ] Élément 3, non-coché
    === "Aperçu"
    <ul>
    <li>
    <label>
    <input type="checkbox" checked>
    Élément 1, coché
    </label>
    </li>
    <li>
    <label>
    <input type="checkbox">
    Élément 2, non-coché
    </label>
    </li>
    <ul>
    <li>
    <label>
    <input type="checkbox" checked>
    Sous-élément 2.1, coché
    </label>
    </li>
    <li>
    <label>
    <input type="checkbox">
    Sous-élément 2.2, non-coché
    </label>
    </li>
    </ul>
    <li>
    <label>
    <input type="checkbox">
    Élément 3, non-coché
    </label>
    </li>
    </ul>

## :fontawesome-solid-quote-right: Citations

!!! abstract ""
    === "Markdown"
            > Voici une citation  
            > sur plusieurs lignes
    === "Aperçu"
        > Voici une citation  
        > sur plusieurs lignes
	
## &mdash; Séparateurs horizontaux

!!! abstract ""
    === "Markdown"
             ---  
             ou *** ou ___ 
    === "Aperçu"
        ---

## :fontawesome-solid-link: Liens

!!! abstract ""
    === "Markdown"
            [Ceci est le texte du lien](https://dcaisson.gitlab.io/formation-basthon/)

            [Ceci est un lien avec un titre au survol](https://dcaisson.gitlab.io/formation-basthon/ "Le titre du lien")
    === "Aperçu"
        [Ceci est le texte du lien](https://dcaisson.gitlab.io/formation-basthon/)

        [Ceci est un lien avec un titre au survol](https://dcaisson.gitlab.io/formation-basthon/ "Le titre du lien")
## :fontawesome-solid-image: Images

!!! abstract ""
    === "Markdown"
            ![logo Markdown](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Markdown"){width=30}
    === "Aperçu"
        ![logo Markdown](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Markdown"){width=30}

## :fontawesome-solid-table: Tableaux

!!! abstract ""
    === "Markdown"
            Titre colonne 1 (gauche) | Titre colonne 2 (centré) | Titre colonne 3 (droite)
            :--- | :---: | ---:
            Cellule 1.1 | Cellule 1.2 | Cellule 1.3
            Cellule 2.1 | Cellule 2.2 | Cellule 2.3
    === "Aperçu"
        Titre colonne 1 (gauche) | Titre colonne 2 (centré) | Titre colonne 3 (droite)
        :--- | :---: | ---:
        Cellule 1.1 | Cellule 1.2 | Cellule 1.3
        Cellule 2.1 | Cellule 2.2 | Cellule 2.3

!!! tip "Astuce"
    Il est cependant plus pratique d'utiliser un site comme [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) pour créer des tableaux.
    
## :fontawesome-solid-terminal: Code

### :material-code-tags: en ligne

!!! abstract ""
    === "Markdown"
            `print("Hello World!")`  
    === "Aperçu"
        `print("Hello World!")`  

### :material-file-code-outline: Bloc de code

!!! abstract ""
    === "Markdown"
            ```python
            print("Hello World!")
            ```
    === "Aperçu"
        ```python
        print("Hello World!")
        ```

## :material-square-root: Formules LaTeX

!!! abstract ""
    === "Markdown"
            Mode en ligne :  
            On cherche les solutions de l'équation (E) : $x^2 + 3x -7 = 0$    
            
            Mode bloc (display) :  
            On trouve :  
            
            $$x = \frac{-3 \pm \sqrt{37}}{2}$$
    === "Aperçu"
        Mode en ligne :  
        On cherche les solutions de l'équation (E) : $x^2 + 3x -7 = 0$    
        
        Mode bloc (display) :  
        On trouve :  
        
        $$x = \frac{-3 \pm \sqrt{37}}{2}$$

## :fontawesome-solid-font: Caractères spécifiques

### &ndash; Tirets

Il n'existe pas de façon spécifique pour réaliser des tirets cadratins ou demi-cadratins. On utilise des balises HTML pour créer de tels tirets.

!!! abstract ""
    === "HTML"
            tiret long ou tiret cadratin &mdash;
	        tiret moyen ou tiret demi-cadratin &ndash;
    === "Aperçu"
        tiret long ou tiret cadratin &mdash;<br>
	    tiret moyen ou tiret demi-cadratin &ndash;
	    
### :fontawesome-solid-keyboard: Touches clavier

On utilise aussi des balises HTML pour créer les touches clavier :

!!! abstract ""
    === "HTML"
            <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd>
    === "Aperçu"
        <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd>
