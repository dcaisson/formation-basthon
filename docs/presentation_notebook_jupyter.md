# Notebooks Jupyter

## Cellules
Un notebook Jupyter composé de plusieurs **cellules**. Certaines d'entre elles peuvent contenir du *texte*, d'autres peuvent contenir du *code Python*.

!!! note "Remarque"
    On peut écrire des formules mathématiques dans les cellules contenant du texte, à l'aide de \(\LaTeX\) (voir l'aide-mémoire \(\LaTeX\) dans la partie ``Ressources``).


## Interpréter du code Python
Le code Python contenu dans une cellule est interprété :  

- en appuyant sur ``Maj + Entrée`` ;  
- en appuyant sur `Ctrl + Entrée` ;  
- ou en cliquant sur l'icône ![icône Run](images/icone_run.png) de la barre d'outils.

## Changer le type d'une cellule
On écrira du texte dans des cellules de type ``Markdown`` et du code Python dans des cellules de type ``Code``. Pour changer le type d'une cellule, on peut utiliser un menu déroulant :

![changer le type d'une cellule](images/changer_type_cellule.png){: .center width=80%}

??? tip "Astuce"
    De nombreux raccourcis clavier existent et permettent de travailler plus rapidement avec des notebooks Jupyter. Par exemple :  
     
    - ``dd`` permet d'effacer une cellule ;  
    - ``a`` et ``b`` permettent d'ajouter une cellule respectivement au-dessus et en dessous de la cellule courante.

## Un exemple
Voici un exemple de notebook Jupyter :

{{ python_carnet('exemple_notebook_jupyter.ipynb') }}

## Avantages et inconvénients

L'installation de Jupyter Notebook peut s'avérer fastidieuse. Certains élèves ne disposent pas d'un ordinateur personnel à la maison, et il leur est parfois interdit d'installer quoi que ce soit sur l'ordinateur familial.

Le [projet Basthon](presentation_basthon.md) permet de créer et partager facilement des notebooks Jupyter sans aucune installation.

